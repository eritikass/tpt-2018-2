const toNumber = require('./toNumber');

module.exports = function (sum1, sum2) {
  return toNumber(sum1) + toNumber(sum2);
};
