const toNumber = require('./toNumber');

test('1 => 1', () => {
  expect(toNumber(1)).toBe(1);
});

test('"1" => 1', () => {
  expect(toNumber('1')).toBe(1);
});

test('"a" is error', () => {
  expect(() => {
    toNumber('a');
  }).toThrow('value \'a\' is not a number!');
});
